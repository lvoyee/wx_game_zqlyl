# wx_game_zqlyl
微信小程序游戏最强连一连Java代码辅助

源码来自：https://blog.csdn.net/lcl1997/article/details/83475388

## 步骤
1. 截屏
2. 分析游戏状态
3. 搜索
4. 自动触摸滑动过关路径



## 开发工具 & 开发环境

idea & jdk1.8


## 更多细节请参考一下两个链接
https://blog.csdn.net/lcl1997/article/details/83475388 （建议先看这个链接）

https://blog.csdn.net/lvoyee/article/details/88599763   (然后在看这个链接)


## FAQ

#### 代码有没有用？
- A： 已经刷完通过了！ 

![lvoyee](https://images.gitee.com/uploads/images/2020/0729/111130_8928ad32_753205.png "屏幕截图.png")

#### 需要准备什么呢？
-A: 看提供的两个文章的连接，里面有详细的描述了！ 不懂的可以加入我们的Q群...
QQ群号 ： 816175200


# wx_game_tiaoyitiao 微信跳一跳

- 添加微信 跳一跳 自动玩源码 

 重新 pull 代码即可获取代码！
 
 同样使用安卓手机，原理也是利用adb 截图分析！
 
 ps: 使用这个代码玩 微信跳一跳不能一只使用，建议自动玩一会然后改用 手动玩，不然会被检测出来！！！

# 参考链接

[adb 无线连接小米手机(免ROOT)](http://www.devcheng.net/post/9e1d1814.html)

[Java+adb命令实现自动刷视频脚本](http://www.devcheng.net/post/58251880.html)
 
作者：伊成

欢迎关注: www.devcheng.net
